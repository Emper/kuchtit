<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('meta_title', 'Recepty')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>

    @yield('head')
</head>
<body>
    <div id="app">

        @include('layouts.nav')

        @yield('content')

    </div>

    @include('layouts.footer')

    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>
