<footer>
    <div class="container">
        <div class="row">
            <p>&copy; {{ date("Y") }} {{ config('app.name') }}</p>
        </div>
    </div>
</footer>
