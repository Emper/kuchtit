<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/img/kuchtit.png" alt="{{ config('app.name', 'CsApp') }}">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav navbar-main">
                <li>
                    <a href="{{ route('search') }}" class="item">Hledat</a>
                </li>
                <li>
                    <a href="{{ route('new.recipes') }}" class="item">Nové recepty</a>
                </li>
                <li>
                    <a href="{{ route('categories') }}" class="item">Kategorie</a>
                </li>
                <li>
                    <a href="{{ route('rankings') }}" class="item">Žebříčky</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}" class="item">Přihlásit se</a></li>
                    <li><a href="{{ route('register') }}" class="item">Registrace</a></li>
                @else
                    @if (Auth::user()->isAdmin())
                        <li><a href="{{ route('admin') }}">Administrace</a></li>
                    @endif
                    @if (Auth::user()->isCook())
                        <li class="dropdown">
                            <a class="item" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Recepty <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('likes.auth') }}"><i class="glyphicon glyphicon-heart"></i> Moje oblíbené</a>
                                </li>
                                <li>
                                    <a href="{{ route('recipes.auth') }}"><i class="glyphicon glyphicon-cutlery"></i> Moje recepty</a>
                                </li>
                                <li>
                                    <a href="{{ route('recipes.create') }}"><i class="glyphicon glyphicon-plus"></i> Nový recept</a>
                                </li>
                                <li>
                                    <a href="{{ route('comments.list') }}"><i class="glyphicon glyphicon-comment"></i> Komentáře</a>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <a class="item" href="{{ route('likes.auth') }}">
                                <i class="glyphicon glyphicon-heart"></i> Moje oblíbené
                            </a>
                        </li>
                    @endif
                    <li class="dropdown">
                        <a class="item" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <img src="{{ Auth::user()->avatarPath() }}" alt="" class="avatar-img">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('user.profile', Auth::user()->slug) }}">
                                    <i class="glyphicon glyphicon-user"></i> Profil
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('settings') }}">
                                    <i class="glyphicon glyphicon-cog"></i> Nastavení
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="glyphicon glyphicon-log-out"></i> Odhlásit se
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
