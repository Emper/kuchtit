@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <h1>Nejnovější recepty</h1>

        @if (! count($recipes))
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Začínáme s oblíbenými recepty</div>

                    <div class="panel-body">
                        <p>Pod účtem <em>{{ Auth::user()->name }}</em> ještě nemáte žádné oblíbené recepty. Začít můžete hned, a to tak, že u konkrétního receptu kliknete na tlačítko <strong>Do oblíbených</strong>.</p>
                    </div>
                </div>
            </div>
        @endif

        @foreach ($recipes as $recipe)
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 col-xxs-12">
                @include('components.recipe', ['recipe' => $recipe])
            </div>
        @endforeach
    </div>

    <div class="text-center center-paginator">
        {{ $recipes->links() }}
    </div>
</div>
@endsection
