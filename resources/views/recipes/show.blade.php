@extends('layouts.app')

@section('meta_title', $recipe->title.' | Recepty')

@section('content')
<div class="container recipe-detail relative">
    <h1>{{ $recipe->title }}</h1>
    <div class="buttons categories">
        @foreach ($recipe->categories as $category)
            <span>{{ $category->name }}</span>
        @endforeach
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-2 relative">
            @include('components.list-box', ['heading' => 'Suroviny', 'data' => $recipe->ingredients])

            @if ($recipe->spices->count())
                @include('components.list-box', ['heading' => 'Koření', 'data' => $recipe->spices])
            @endif
        </div>
        <div class="col-sm-6 col-md-5 col-lg-4 relative">
            <img src="{{ $recipe->thumbnailPath() }}" alt="" class="img-responsive thumbnail">
            <div class="buttons extra">
                @if ($recipe->duration)
                    <span><i class="glyphicon glyphicon-time"></i>{{ $recipe->duration->name }}</span>
                @endif
                @if ($recipe->difficulty)
                    <span><i class="glyphicon glyphicon-stats"></i>{{ $recipe->difficulty->name }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="panel panel-default how-to">
                <div class="panel-heading">Postup přípravy</div>
                <div class="panel-body">
                    @foreach(explode(PHP_EOL, $recipe->body) as $paragraph)
                        <p>{{{ $paragraph }}}</p>
                    @endforeach
                    <hr>
                    <div class="big-buttons">
                        @if (Auth::user() && Auth::user()->canEditRecipe($recipe))
                            <a href="{{ route('recipes.edit', $recipe->id) }}" class="btn btn-primary">
                                <i class="glyphicon glyphicon-pencil"></i>Upravit
                            </a>
                        @endif
                        @if (Auth::user())
                            <form method="POST" action="{{ route('recipes.like', $recipe->id) }}" class="inline" >
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary">
                                    <i class="glyphicon glyphicon-heart{{ ! $recipe->liked() ? '-empty' : '' }}"></i>
                                    {{ $recipe->liked() ? 'V oblíbených' : 'Do oblíbených' }}
                                </button>
                            </form>
                        @else
                            <a href="{{ route('login') }}" class="btn btn-primary">
                                <i class="glyphicon glyphicon-heart-empty"></i>Do oblíbených
                            </a>
                        @endif
                    </div>
                    <hr>
                    @if ($recipe->user)
                        <p class="by-user">Autor:
                            <a href="{{ route('user.profile', $recipe->user->slug) }}">
                                {{ $recipe->user->name }}
                            </a>
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="komentare">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="panel panel-default how-to">
                <div class="panel-heading">Komentáře</div>
                <div class="panel-body">
                @include('components.flash')
                    @if (! $comments->count())
                        <p>K tomuto receptu ještě nikdo komentář nenapsal. Buď první!</p>
                    @else
                        @foreach ($comments as $comment)
                            @include('components.comment', ['comment' => $comment])
                        @endforeach
                    @endif
                    <div class="small-paginator comment-paginator">
                        {{ $comments->links() }}
                    </div>
                    <hr class="clear">
                    @if (Auth::user() && Auth::user()->verified)
                        <form role="form" method="POST" action="{{ route('comments.store', $recipe->id) }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                <label for="body">Napište váš komentář:</label>
                                <textarea name="body" id="body" class="form-control" required>{{ old('body') }}</textarea>
                                @if ($errors->has('body'))
                                    <span class="help-block"><strong>{{ $errors->first('body') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Odeslat komentář</button>
                            </div>
                        </form>
                    @elseif (Auth::user() && !Auth::user()->verified)
                        <p>Pro psaní komentářů musíte ověřit svou <a href="{{ route('settings.email') }}">e-mailovou adresu</a>.</p>
                    @else
                        <p>K psaní komentářů se musíte <a href="{{ route('login') }}">přihlásit</a>.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container similar">
    @if ($similar = $recipe->similarRecipes(4))
        <div class="row">
            <h3>Podobné recepty</h3>
            @foreach ($similar as $recipe)
                <div class="col-lg-3 col-sm-4 col-xs-6 col-xxs-12{{ $loop->last ? ' hidden-md hidden-sm' : '' }}">
                    @include('components.recipe', ['recipe' => $recipe])
                </div>
            @endforeach
        </div>
    @endif
</div>
@endsection
