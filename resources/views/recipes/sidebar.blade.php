<div class="col-md-3">
    <ul class="list-group">
        <li class="list-group-item list-group-link">
            <a href="{{ route('recipes.auth') }}">Moje recepty</a>
        </li>
        <li class="list-group-item list-group-link">
            <a href="{{ route('recipes.create') }}">Nový recept</a>
        </li>
        <li class="list-group-item list-group-link">
            <a href="{{ route('comments.list') }}">Komentáře</a>
        </li>
    </ul>
</div>
