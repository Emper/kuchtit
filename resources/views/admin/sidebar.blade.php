<div class="col-md-3">
    <ul class="list-group">
        <li class="list-group-item list-group-link">
            <a href="{{ route('admin') }}">Statistiky</a>
        </li>
        <li class="list-group-item list-group-link">
            <a href="{{ route('admin.users') }}">Uživatelé</a>
        </li>
        <li class="list-group-item list-group-link">
            <a href="{{ route('admin.comments') }}">Komentáře</a>
        </li>
        <li class="list-group-item list-group-link">
            <a href="{{ route('admin.recipes') }}">Recepty</a>
        </li>
        <li class="list-group-item list-group-link">
            <a href="{{ route('admin.categories') }}">Kategorie</a>
        </li>
        <li class="list-group-item list-group-link">
            <a href="{{ route('admin.ingredients') }}">Suroviny</a>
        </li>
        <li class="list-group-item list-group-link">
            <a href="{{ route('admin.spices') }}">Koření</a>
        </li>
    </ul>
</div>
