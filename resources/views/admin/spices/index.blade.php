@extends('layouts.admin')

@section('heading', 'Koření')

@section('body')

    @include('admin.table', ['table' => 'spices', 'data' => $spices])

    <div class="bottom-buttons">
        <div class="form-group">
            <a class="btn btn-primary" href="{{ route('admin.spices.create') }}">Vytvořit nové koření</a>
        </div>
        <div class="pull-right">
            {{ $spices->links() }}
        </div>
    </div>

@endsection
