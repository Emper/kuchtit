<tr>
    <td class="header">
        <a href="{{ $url }}">
            <img src="{{ url('/img/kuchtit-md.png') }}" alt="{{ $slot }}" title="{{ $slot }}">
        </a>
    </td>
</tr>
