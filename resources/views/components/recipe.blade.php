<div class="panel recipe">
    <a class="link" href="{{ route('recipes.show', $recipe->slug) }}">
        <img src="{{ $recipe->thumbnailPath() }}" alt="" class="img-responsive">
        @if ($recipe->liked())
            <i class="glyphicon glyphicon-heart"></i>
        @endif
        <div class="info">
            <div class="heading">
                {{ $recipe->title }}
            </div>
            <div class="categories">
                {{ implode($recipe->categories->pluck('name')->toArray(), ', ') }}
            </div>
        </div>
    </a>
</div>
