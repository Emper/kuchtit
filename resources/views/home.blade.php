@extends('layouts.app')

@section('meta_title', 'Co dnes uvařit? | Recepty')

@section('head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">

    @include('components.flash')

    <div class="centered-outer homepage">
        <div class="centered-middle">
            <h1>Zjisti, co můžeš uvařit</h1>
            <form class="centered-inner form-group" action="" method="post">
                {{ csrf_field() }}

                <select name="ingredients[]" id="ingredients" class="select form-control" multiple="multiple">
                    @foreach ($ingredients as $ingredient)
                        <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                    @endforeach
                </select>

                <button id="search-button" type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-search"></i> Najít recepty
                </button>
            </form>

            <div class="explanation">
              <p>Napiš, co jsi našel v ledničce, ve spíži, nebo třeba ve sklepě a&nbsp;zjisti si recepty jídel, které z toho můžeš vyrobit!</p>
            </div>

        </div>
    </div>

    <div class="row home-bottom">
        <h3>Oblíbené recepty</h3>
        @foreach ($favourites as $recipe)
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 col-xxs-12">
                @include('components.recipe', ['recipe' => $recipe])
            </div>
        @endforeach
    </div>
    <br>
    <div class="row">
        <h3>Nové recepty</h3>
        @foreach ($fresh as $recipe)
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 col-xxs-12">
                @include('components.recipe', ['recipe' => $recipe])
            </div>
        @endforeach
    </div>
</div>
@endsection


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
        $('select').select2({
            theme: "bootstrap"
        });
    </script>
@endsection

